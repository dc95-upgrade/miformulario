# Propósito

Este repositorio tiene el propósito de que trabajéis con ramas y con merges sencillos en git.


## Intrucciones

* Hemos descubierto que se están registrando usuarios sin aceptar las condiciones legales, y esto puede provocar multitud de denuncias a nuestra empresa.
¡Necesitamos urgentemente que se solucione este problema.! (crearos una rama que lo solucione, **mergeadla en master** y **crear un nuevo tag**)

* Una vez que hayais arreglado el problema y esté mergeado, queremos poder registrar la fecha de nacimiento de nuestros usuarios. (**crearos una rama específica** para esta nueva funcionalidad)

## Para ejecutarlo

Solamente necesitamos tener un servidor web ejecutandose en local y que pueda procesar PHP.

Podéis hacerlo si tenéis un stack XAMP/LAMP, o bien levantando un contenedor con todo lo necesario.
Si optais por esto último os hemos dejado un fichero docker-compose.yml que contiene todo lo necesario para que podáis ejecutar esta aplicación.

Para levantarlo es tan sencillo como ejecutar:

```
docker-compose up -d
```

Y ya podréis acceder a través de localhost:8080 

**NOTA:** Mirad a ver que no tengáis el puerto ocupado con otros procesos.